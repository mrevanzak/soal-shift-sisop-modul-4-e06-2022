# SISOP Modul 4

## Daftar isi

- [Anggota Kelompok](#anggota-kelompok)
- [Nomer 1](#nomer-1)
  - [Soal 1.A](#1a)
  - [Soal 1.B](#1b)
  - [Soal 1.C](#1c)
  - [Soal 1.D](#1d)
  - [Soal 1.E](#1e)
  - [Hasil](#1hasil)
- [Nomer 2](#nomer-2)
  - [Soal 2.A](#2a)
  - [Soal 2.B](#2b)
  - [Soal 2.C](#2c)
  - [Soal 2.D](#2d)
  - [Soal 2.E](#2e)
  - [Hasil](#2hasil)
- [Nomer 3](#nomer-3)
  - [Soal 3.A](#3a)
  - [Soal 3.B](#3b)
  - [Soal 3.C](#3c)
  - [Soal 3.D](#3d)
  - [Soal 3.E](#3e)
  - [Hasil](#3hasil)
- [Kendala](#kendala)

## Anggota Kelompok

| NRP        | NAMA                       |
| ---------- | -------------------------- |
| 5025201260 | Fadel Pramaputra Maulana   |
| 5025201079 | Julio Geraldi Soeiono      |
| 5025201145 | Mochamad Revanza Kurniawan |

## Nomer 1
Anya adalah adalah seorang programmer wibu yang suka mengoleksi anime. Dia sangat senang membuat program yang dikolaborasikan dengan anime. Suatu hari, Anya sedang ingin membuat system dengan ketentuan berikut:

### 1.A
Semua direktori dengan awalan “Animeku_” akan terencode dengan ketentuan semua file yang terdapat huruf besar akan ter encode dengan atbash cipher dan huruf kecil akan terencode dengan rot13
	
	Contoh : 
	“Animeku_/anya_FORGER.txt” → “Animeku_/naln_ULITVI.txt”

Untuk fungsi encode dan decodenya adalah sebagai berikut 
```c
void encode1(char *str){
    if (strcmp(str, ".") == 0 || strcmp(str, "..") == 0)
        return;

    int strLength = strlen(str);
    for (int i = 0; i < strLength; i++){
        if (str[i] == '/')
            continue;
        if (str[i] == '.')
            break;

        if (str[i] >= 'A' && str[i] <= 'Z')
            str[i] = 'Z' + 'A' - str[i];
        if (str[i] >= 'a' && str[i] <= 'z')
            str[i] = 'a' + (str[i] - 'a' + 13) % 26;
    }
}

void decode1(char *str){
    if (strcmp(str, ".") == 0 || strcmp(str, "..") == 0 || strstr(str, "/") == NULL)
        return;

    int strLength = strlen(str), s = 0;
    for (int i = strLength; i >= 0; i--){
        if (str[i] == '/')
            break;

        if (str[i] == '.'){
            strLength = i;
            break;
        }
    }
    for (int i = 0; i < strLength; i++){
        if (str[i] == '/'){
            s = i + 1;
            break;
        }
    }
    for (int i = s; i < strLength; i++){
        if (str[i] == '/')
            continue;
        if (str[i] >= 'A' && str[i] <= 'Z')
            str[i] = 'Z' + 'A' - str[i];
        if (str[i] >= 'a' && str[i] <= 'z')
            str[i] = 'a' + (str[i] - 'a' + 13) % 26;
    }
}
```
### 1.B
Semua direktori di-rename dengan awalan “Animeku_”, maka direktori tersebut akan menjadi direktori ter-encode dengan ketentuan sama dengan 1a.

agar yang ada di dalam suatu direktori menjadi ter-encode masukkan fungsi encode ke dalam fuse operator readdir
```c
static int xmp_readdir(const char *path, void *buf, fuse_fill_dir_t filler, off_t offset, struct fuse_file_info *fi){
    char *strCheck; int flag;
    if ((strCheck = strstr(path, prefix[0])) != NULL){
        flag = 1;
        if (strCheck != NULL)
            decode1(strCheck);
    }else if ((strCheck = strstr(path, prefix[1])) != NULL){
        flag = 2;
        if (strCheck != NULL)
            decode2(strCheck, 0);
    }

    char fpath[1000];
    if (strcmp(path, "/") == 0){
        path = dirPath;
        sprintf(fpath, "%s", path);
    }
    else
        sprintf(fpath, "%s%s", dirPath, path);

    int result = 0;
    DIR *dp;
    struct dirent *dir;
    (void)offset;
    (void)fi;

    dp = opendir(fpath);
    if (dp == NULL)
        return -errno;

    while ((dir = readdir(dp)) != NULL){ 
        struct stat st;
        memset(&st, 0, sizeof(st));
        st.st_ino = dir->d_ino;
        st.st_mode = dir->d_type << 12;
        if (strCheck != NULL){
            if (flag == 1) encode1(dir->d_name);
            else if (flag == 2) encode2(dir->d_name);
        }
        result = (filler(buf, dir->d_name, &st, 0));
        if (result != 0)
            break;
    }
    closedir(dp);
    return 0;
}
```

### 1.C
Apabila direktori yang terenkripsi di-rename menjadi tidak ter-encode, maka isi direktori tersebut akan terdecode.

Untuk soal ini sudah include ke dalam fungsi yang [soal 1.B](#1b)

### 1.D
Setiap data yang terencode akan masuk dalam file “Wibu.log” 
Contoh isi: 
RENAME terenkripsi /home/[USER]/Downloads/hai --> /home/[USER]/Downloads/Animeku_hebat 
RENAME terdecode /home/[USER]/Downloads/Animeku_ --> /home/[USER]/Downloads/Coba

berikut fungsi untuk menuliskan ke dalam file Wibu.log
```c
void wibuLog(const char *old, char *new, int type){
    char *logName = "Wibu.log";
    char logPath[100];
    sprintf(logPath, "%s/%s", dirPath, logName);
    FILE *logFile = fopen(logPath, "a");

    if (type == 0)
        fprintf(logFile, "MKDIR\tterenkripsi\t%s\t->\t%s\n", old, new);
    else if (type == 1)
        fprintf(logFile, "RENAME\tterenkripsi\t%s\t->\t%s\n", old, new);
    else if (type == 2)
        fprintf(logFile, "RENAME\tterdecode\t%s\t->\t%s\n", old, new);
    fclose(logFile);
}
```

panggil fungsi tersebut ketika merename suatu file
```c
static int xmp_rename(const char *source, const char *dest){
    int flag;
    if (strstr(source, prefix[0]) != NULL){
        flag = 1;
    }else if (strstr(source, prefix[1]) != NULL){
        flag = 2;
    }
    char fileSource[1000], fileDest[1000];
    sprintf(fileSource, "%s%s", dirPath, source);
    sprintf(fileDest, "%s%s", dirPath, dest);
    int result;
    if (flag == 1){
        if (strstr(fileDest, prefix[0]) != NULL)
            wibuLog(fileSource, fileDest, 1);
        else if (strstr(fileSource, prefix[0]) != NULL)
            wibuLog(fileSource, fileDest, 2);
        result = rename(fileSource, fileDest);
    }else if(flag == 2){
        char *strCheck; 
        strCheck = strstr(fileSource, prefix[1]);
        if (strCheck != NULL)
            decode2(strCheck, 0);
        strCheck = strstr(fileDest, prefix[1]);
        if (strCheck != NULL)
            decode2(strCheck, 0);
        printf("RENAME-------\n");
        printf("%s\n", fileSource);
        printf("%s\n", fileDest);
        if ((result = rename(fileSource, fileDest)) == 0){
            char temp[2*v];
            sprintf(temp, "RENAME::%s::%s", fileSource, fileDest);
            hayolongapainLog(temp, INFO);
        }
    }

    if (result == -1)
        return -errno;

    return 0;
}
```

### 1.E
Metode encode pada suatu direktori juga berlaku terhadap direktori yang ada di dalamnya.(rekursif)

Untuk soal ini sudah include ke dalam fungsi yang [soal 1.B](#1b)

### 1.Hasil
![image](/uploads/e355fe91d32bf0d92cc49e92984e71cc/image.png)
![image](/uploads/3e978a837d896c6004d095ae0e63a27d/image.png)
![image](/uploads/2769767c54454fb82cda8300c0c1b8db/image.png)

## Nomer 2
Saat Anya sedang sibuk mengerjakan programnya, tiba-tiba Innu datang ke rumah Anya untuk mengembalikan barang yang dipinjamnya. Innu adalah programmer jenius sekaligus teman Anya. Ia melihat program yang dibuat oleh Anya dan membantu Anya untuk menambahkan fitur pada programnya dengan ketentuan sebagai berikut : 

### 2.A 
Jika suatu direktori dibuat dengan awalan “IAN_[nama]”, maka seluruh isi dari direktori tersebut akan terencode dengan algoritma Vigenere Cipher dengan key “INNUGANTENG” (Case-sensitive, Vigenere).

```bash
void encode2(char *str){
    if (strcmp(str, ".") == 0 || strcmp(str, "..") == 0)
        return;

    int strLength = strlen(str);

    printf("enc: %s\nenc: ", str);
    for (int i=0; i < strLength; i++){
    printf("%c", str[i]);
    }printf("\n");

    char key[] = "INNUGANTENG";
    int keyLen = strlen(key), i, j;
    char newKey[strLength];
    //new key
    for(i = 0, j = 0; i < strLength; i++, j++){
        if(j == keyLen) j = 0;
        newKey[i] = key[j];
    } newKey[i] = '\0';
    //encryption
    for(i = 0, j = 0; i < strLength; i++, j++){
        if (islower(str[i])) str[i] = tolower(((toupper(str[i]) + newKey[j]) % 26) + 'A');
        else if (isupper(str[i])) str[i] = ((str[i] + newKey[j]) % 26) + 'A';
        else {
            j--; 
            continue;
        }
    } 
}

void decode2(char *str, int type){
    if (strcmp(str, ".") == 0 || strcmp(str, "..") == 0 || strstr(str, "/") == NULL)
        return;

    int strLength = strlen(str), s = 0, end = strLength;
    for (int i = 0; i < strLength; i++){
        if (str[i] == '/'){
            s = i + 1;
            break;
        }
    }

    if (type == 1){
        for (int i=end-1; i>=0; i--){
            if (str[i] == '/'){
                end = i;
                break;
            }
        }
    }

    printf("dec: %s\ndec: ", str);
    for (int i=s; i < end; i++){
        printf("%c", str[i]);
    }printf("\n");

    char key[] = "INNUGANTENG";
    int keyLen = strlen(key), i, j;
    char newKey[strLength];
    //new key
    for(i = s, j = 0; i < end; i++, j++){
        if(j == keyLen) j = 0;
        newKey[i] = key[j];
    } newKey[i] = '\0';
    //decryption
    for(i = s, j = s; i < end; i++, j++){
        if (islower(str[i])) str[i] = tolower((((toupper(str[i]) - newKey[j]) + 26) % 26) + 'A');
        else if (str[i] == '/') {
            j = s-1;
            continue;
        }
        else if (isupper(str[i])) str[i] = (((str[i] - newKey[j]) + 26) % 26) + 'A';
        else {
            j--;
            continue;
        }
    } 
}
```

Kode diatas merupakan algoritma encode dan decode dari vignere chipher menggunakan key yaitu INNUGANTENG

### 2.B 
Jika suatu direktori di rename dengan “IAN_[nama]”, maka seluruh isi dari direktori tersebut akan terencode seperti pada no. 2a.

Agar yang ada di dalam suatu direktori menjadi ter-encode masukkan fungsi encode ke dalam fuse operator readdir pada flag 2.

```bash
static int xmp_readdir(const char *path, void *buf, fuse_fill_dir_t filler, off_t offset, struct fuse_file_info *fi){
    char *strCheck; int flag;
    if ((strCheck = strstr(path, prefix[0])) != NULL){
        flag = 1;
        if (strCheck != NULL)
            decode1(strCheck);
    }else if ((strCheck = strstr(path, prefix[1])) != NULL){
        flag = 2;
        if (strCheck != NULL)
            decode2(strCheck, 0);
    }

    char fpath[1000];
    if (strcmp(path, "/") == 0){
        path = dirPath;
        sprintf(fpath, "%s", path);
    }
    else
        sprintf(fpath, "%s%s", dirPath, path);

    int result = 0;
    DIR *dp;
    struct dirent *dir;
    (void)offset;
    (void)fi;

    dp = opendir(fpath);
    if (dp == NULL)
        return -errno;

    while ((dir = readdir(dp)) != NULL){ 
        struct stat st;
        memset(&st, 0, sizeof(st));
        st.st_ino = dir->d_ino;
        st.st_mode = dir->d_type << 12;
        if (strCheck != NULL){
            if (flag == 1) encode1(dir->d_name);
            else if (flag == 2) encode2(dir->d_name);
        }
        result = (filler(buf, dir->d_name, &st, 0));
        if (result != 0)
            break;
    }
    closedir(dp);
    return 0;
}
```

### 2.C
Apabila nama direktori dihilangkan “IAN_”, maka isi dari direktori tersebut akan terdecode berdasarkan nama aslinya.

Untuk soal ini sudah include ke dalam fungsi yang soal 1.B

### 2.D
Untuk memudahkan dalam memonitor kegiatan yang dilakukan pada filesystem yang telah dibuat, ia membuat log system pada direktori “/home/[user]/hayolongapain_[kelompok].log”. Dimana log ini akan menyimpan daftar perintah system call yang telah dijalankan pada filesystem.

Berikut fungsi untuk menuliskan ke dalam file hayolongapainLog.log

```bash
void hayolongapainLog(char *str, int type){
    char logPath[100];
    sprintf(logPath, "%s/hayolongapain_E06.log", dirPath);
    
    FILE *logFile = fopen(logPath, "a");
    time_t t = time(NULL);
    struct tm time = *localtime(&t);
    if (type == INFO)
        fprintf(logFile, "INFO::%02d%02d%04d-%02d:%02d:%02d::%s\n", time.tm_mday, time.tm_mon + 1, time.tm_year + 1900, time.tm_hour, time.tm_min, time.tm_sec, str);
    else if (type == WARNING)
        fprintf(logFile, "WARNING::%02d%02d%04d-%02d:%02d:%02d::%s\n", time.tm_mday, time.tm_mon + 1, time.tm_year + 1900, time.tm_hour, time.tm_min, time.tm_sec, str);
    fclose(logFile);
}
```

### 2.E
Karena Innu adalah seorang perfeksionis, ia membagi isi dari log systemnya menjadi 2 level, yaitu level INFO dan WARNING. Untuk log level WARNING, digunakan untuk mencatat syscall rmdir dan unlink. Sisanya, akan dicatat pada level INFO dengan format sebagai berikut : 

```bash
static int xmp_mkdir(const char *path, mode_t mode){
    ...
    }else if (strstr(path, prefix[1]) != NULL){
        ...
        char temp[v];
        sprintf(temp, "MKDIR::%s", fpath);
        if (folderPath != NULL)
            hayolongapainLog(temp, INFO);
    ...
}

static int xmp_mknod(const char *path, mode_t mode, dev_t rdev){
    ...
    if (result != -1  && flag == 2){
        char temp[v];
        sprintf(temp, "MKNOD::%s", fpath);
        hayolongapainLog(temp, INFO);
    }       
    ...
}

static int xmp_rmdir(const char *path){
    ...
    else if (flag == 2){
        if ((result = rmdir(fpath)) == 0){
            char temp[2*v];
            sprintf(temp, "REMOVE::%s", fpath);
            hayolongapainLog(temp, WARNING);
        }
    }
    ...
}

static int xmp_rename(const char *source, const char *dest){
    ...
    }else if(flag == 2){
        ...
        if ((result = rename(fileSource, fileDest)) == 0){
            char temp[2*v];
            sprintf(temp, "RENAME::%s::%s", fileSource, fileDest);
            hayolongapainLog(temp, INFO);
        }
    }
    ...
}
```

### 2.Hasil
![image](/uploads/f34c70fcd27ca8b0fe69af284aaeca97/image.png)

![image](/uploads/8b8f59cdd0fbd1f1ba6e534198e55fc7/image.png)

## Kendala
1. Tidak bisa buka save file .txt yang ada didalam folder terencrypt
2. Tidak bisa rename folder/file yang ada didalam folder terencrypt
3. Kadang tidak bisa membuka file fuse "permission denied"
