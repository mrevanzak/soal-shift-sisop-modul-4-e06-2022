#define FUSE_USE_VERSION 28
#include <fuse.h>
#include <time.h>
#include <ctype.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <unistd.h>
#include <fcntl.h>
#include <dirent.h>
#include <errno.h>
#include <sys/time.h>
#include <sys/wait.h>
#include <stdbool.h>
#define v 1024
#define INFO 1
#define WARNING -1

static const char *dirPath = "/home/fadelpm2002/Documents";
char prefix[2][10] = {"Animeku_", "IAN_"};

void hayolongapainLog(char *str, int type){
    char logPath[100];
    sprintf(logPath, "%s/hayolongapain_E06.log", dirPath);
    
    FILE *logFile = fopen(logPath, "a");
    time_t t = time(NULL);
    struct tm time = *localtime(&t);
    if (type == INFO)
        fprintf(logFile, "INFO::%02d%02d%04d-%02d:%02d:%02d::%s\n", time.tm_mday, time.tm_mon + 1, time.tm_year + 1900, time.tm_hour, time.tm_min, time.tm_sec, str);
    else if (type == WARNING)
        fprintf(logFile, "WARNING::%02d%02d%04d-%02d:%02d:%02d::%s\n", time.tm_mday, time.tm_mon + 1, time.tm_year + 1900, time.tm_hour, time.tm_min, time.tm_sec, str);
    fclose(logFile);
}

void wibuLog(const char *old, char *new, int type){
    char *logName = "Wibu.log";
    char logPath[100];
    sprintf(logPath, "%s/%s", dirPath, logName);
    FILE *logFile = fopen(logPath, "a");

    if (type == 0)
        fprintf(logFile, "MKDIR\tterenkripsi\t%s\t->\t%s\n", old, new);
    else if (type == 1)
        fprintf(logFile, "RENAME\tterenkripsi\t%s\t->\t%s\n", old, new);
    else if (type == 2)
        fprintf(logFile, "RENAME\tterdecode\t%s\t->\t%s\n", old, new);
    fclose(logFile);
}

void encode1(char *str){
    if (strcmp(str, ".") == 0 || strcmp(str, "..") == 0)
        return;

    int strLength = strlen(str);
    for (int i = 0; i < strLength; i++){
        if (str[i] == '/')
            continue;
        if (str[i] == '.')
            break;

        if (str[i] >= 'A' && str[i] <= 'Z')
            str[i] = 'Z' + 'A' - str[i];
        if (str[i] >= 'a' && str[i] <= 'z')
            str[i] = 'a' + (str[i] - 'a' + 13) % 26;
    }
}

void decode1(char *str){
    if (strcmp(str, ".") == 0 || strcmp(str, "..") == 0 || strstr(str, "/") == NULL)
        return;

    int strLength = strlen(str), s = 0;
    for (int i = strLength; i >= 0; i--){
        if (str[i] == '/')
            break;

        if (str[i] == '.'){
            strLength = i;
            break;
        }
    }
    for (int i = 0; i < strLength; i++){
        if (str[i] == '/'){
            s = i + 1;
            break;
        }
    }
    for (int i = s; i < strLength; i++){
        if (str[i] == '/')
            continue;
        if (str[i] >= 'A' && str[i] <= 'Z')
            str[i] = 'Z' + 'A' - str[i];
        if (str[i] >= 'a' && str[i] <= 'z')
            str[i] = 'a' + (str[i] - 'a' + 13) % 26;
    }
}

void encode2(char *str){
    if (strcmp(str, ".") == 0 || strcmp(str, "..") == 0)
        return;

    int strLength = strlen(str);

    printf("enc: %s\nenc: ", str);
    for (int i=0; i < strLength; i++){
    printf("%c", str[i]);
    }printf("\n");

    char key[] = "INNUGANTENG";
    int keyLen = strlen(key), i, j;
    char newKey[strLength];
    //new key
    for(i = 0, j = 0; i < strLength; i++, j++){
        if(j == keyLen) j = 0;
        newKey[i] = key[j];
    } newKey[i] = '\0';
    //encryption
    for(i = 0, j = 0; i < strLength; i++, j++){
        if (islower(str[i])) str[i] = tolower(((toupper(str[i]) + newKey[j]) % 26) + 'A');
        else if (isupper(str[i])) str[i] = ((str[i] + newKey[j]) % 26) + 'A';
        else {
            j--; 
            continue;
        }
    } 
}

void decode2(char *str, int type){
    if (strcmp(str, ".") == 0 || strcmp(str, "..") == 0 || strstr(str, "/") == NULL)
        return;

    int strLength = strlen(str), s = 0, end = strLength;
    for (int i = 0; i < strLength; i++){
        if (str[i] == '/'){
            s = i + 1;
            break;
        }
    }

    if (type == 1){
        for (int i=end-1; i>=0; i--){
            if (str[i] == '/'){
                end = i;
                break;
            }
        }
    }

    printf("dec: %s\ndec: ", str);
    for (int i=s; i < end; i++){
        printf("%c", str[i]);
    }printf("\n");

    char key[] = "INNUGANTENG";
    int keyLen = strlen(key), i, j;
    char newKey[strLength];
    //new key
    for(i = s, j = 0; i < end; i++, j++){
        if(j == keyLen) j = 0;
        newKey[i] = key[j];
    } newKey[i] = '\0';
    //decryption
    for(i = s, j = s; i < end; i++, j++){
        if (islower(str[i])) str[i] = tolower((((toupper(str[i]) - newKey[j]) + 26) % 26) + 'A');
        else if (str[i] == '/') {
            j = s-1;
            continue;
        }
        else if (isupper(str[i])) str[i] = (((str[i] - newKey[j]) + 26) % 26) + 'A';
        else {
            j--;
            continue;
        }
    } 
}

static int xmp_getattr(const char *path, struct stat *stbuf){
    printf("getattr\t%s\n", path);
    char *strCheck;
    if ((strCheck = strstr(path, prefix[0])) != NULL){
        if (strCheck != NULL)
            decode1(strCheck);
    }else if ((strCheck = strstr(path, prefix[1])) != NULL){
        if (strCheck != NULL)
            decode2(strCheck, 0);
    }

    int result;
    char fpath[1000];

    sprintf(fpath, "%s%s", dirPath, path);
    printf("getattr: %s\n", fpath);
    result = lstat(fpath, stbuf);

    if (result == -1)
        return -errno;

    return 0;
}

static int xmp_readdir(const char *path, void *buf, fuse_fill_dir_t filler, off_t offset, struct fuse_file_info *fi){
    char *strCheck; int flag;
    if ((strCheck = strstr(path, prefix[0])) != NULL){
        flag = 1;
        if (strCheck != NULL)
            decode1(strCheck);
    }else if ((strCheck = strstr(path, prefix[1])) != NULL){
        flag = 2;
        if (strCheck != NULL)
            decode2(strCheck, 0);
    }

    char fpath[1000];
    if (strcmp(path, "/") == 0){
        path = dirPath;
        sprintf(fpath, "%s", path);
    }
    else
        sprintf(fpath, "%s%s", dirPath, path);

    int result = 0;
    DIR *dp;
    struct dirent *dir;
    (void)offset;
    (void)fi;

    dp = opendir(fpath);
    if (dp == NULL)
        return -errno;

    while ((dir = readdir(dp)) != NULL){ 
        struct stat st;
        memset(&st, 0, sizeof(st));
        st.st_ino = dir->d_ino;
        st.st_mode = dir->d_type << 12;
        if (strCheck != NULL){
            if (flag == 1) encode1(dir->d_name);
            else if (flag == 2) encode2(dir->d_name);
        }
        result = (filler(buf, dir->d_name, &st, 0));
        if (result != 0)
            break;
    }
    closedir(dp);
    return 0;
}

static int xmp_read(const char *path, char *buf, size_t size, off_t offset, struct fuse_file_info *fi){
    char *strCheck;
    if ((strCheck = strstr(path, prefix[0])) != NULL){
        if (strCheck != NULL)
            decode1(strCheck);
    }else if ((strCheck = strstr(path, prefix[1])) != NULL){
        if (strCheck != NULL)
            decode2(strCheck, 0);
    }

    char fpath[1000];
    if (strcmp(path, "/") == 0){
        path = dirPath;
        sprintf(fpath, "%s", path);
    }
    else
        sprintf(fpath, "%s%s", dirPath, path);

    int result = 0;
    int fd = 0;
    (void)fi;

    fd = open(fpath, O_RDONLY);
    if (fd == -1)
        return -errno;

    result = pread(fd, buf, size, offset);
    if (result == -1)
        result = -errno;

    close(fd);
    return result;
}

static int xmp_mkdir(const char *path, mode_t mode){
    if (strstr(path, prefix[0]) != NULL){
        char fpath[1000];
        if (strcmp(path, "/") == 0){
            path = dirPath;
            sprintf(fpath, "%s", path);
        }
        else sprintf(fpath, "%s%s", dirPath, path);
        int result = mkdir(fpath, mode);
        char *folderPath = strstr(path, prefix[0]);
        if (folderPath != NULL)
            wibuLog(fpath, fpath, 0);
        if (result == -1)
            return -errno;
    }else if (strstr(path, prefix[1]) != NULL){
        char *strCheck = strstr(path, prefix[1]);
        if (strCheck != NULL)
            decode2(strCheck, 1);
        char fpath[1000];
        if (strcmp(path, "/") == 0){
            path = dirPath;
            sprintf(fpath, "%s", path);
        }
        else sprintf(fpath, "%s%s", dirPath, path);
        int result = mkdir(fpath, mode);
        char *folderPath = strstr(path, prefix[1]);
        char temp[v];
        sprintf(temp, "MKDIR::%s", fpath);
        if (folderPath != NULL)
            hayolongapainLog(temp, INFO);
        if (result == -1)
            return -errno;
    }
    return 0;
}

static int xmp_mknod(const char *path, mode_t mode, dev_t rdev){
    char fpath[1000];
    if (strcmp(path, "/") == 0){
        path = dirPath;
        sprintf(fpath, "%s", path);
    }
    else
        sprintf(fpath, "%s%s", dirPath, path);
//    printf("MKNOD::BEFORE::%s\n", fpath);

    int flag;
    if (strstr(path, prefix[0]) != NULL){
        flag = 1;
    }else if (strstr(path, prefix[1]) != NULL){
        flag = 2;
    }
    int result;
    char *strCheck;
    if (flag == 2){
        strCheck = strstr(fpath, prefix[1]);
        decode2(strCheck, 1);
    }
//    printf("MKNOD::AFTER::%s\n", fpath);
    if (S_ISREG(mode)){
        result = open(fpath, O_CREAT | O_EXCL | O_WRONLY, mode);
        if (result >= 0)
            result = close(result);
    }
    else if (S_ISFIFO(mode))
        result = mkfifo(fpath, mode);
    else
        result = mknod(fpath, mode, rdev);
    
    if (result != -1  && flag == 2){
        char temp[v];
        sprintf(temp, "MKNOD::%s", fpath);
        hayolongapainLog(temp, INFO);
    }       

    if (result == -1)
        return -errno;

    return 0;
}

static int xmp_unlink(const char *path){
    char *strCheck = strstr(path, prefix[0]);
    if (strCheck != NULL)
        decode1(strCheck);

    char fpath[1000];
    if (strcmp(path, "/") == 0){
        path = dirPath;
        sprintf(fpath, "%s", path);
    }
    else
        sprintf(fpath, "%s%s", dirPath, path);

    int result;
    result = unlink(fpath);
    if (result == -1)
        return -errno;

    return 0;
}

static int xmp_rmdir(const char *path){
    char *strCheck; int flag;
    if ((strCheck = strstr(path, prefix[0])) != NULL){
        flag = 1;
        if (strCheck != NULL)
            decode1(strCheck);
    }else if ((strCheck = strstr(path, prefix[1])) != NULL){
        flag = 2;
        if (strCheck != NULL)
            decode2(strCheck, 0);
    }

    char fpath[1000];
    sprintf(fpath, "%s%s", dirPath, path);
    int result;
    if (flag == 1){
        result = rmdir(fpath);
    }
    else if (flag == 2){
        if ((result = rmdir(fpath)) == 0){
            char temp[2*v];
            sprintf(temp, "REMOVE::%s", fpath);
            hayolongapainLog(temp, WARNING);
        }
    }

    if (result == -1)
        return -errno;

    return 0;
}

static int xmp_rename(const char *source, const char *dest){
    int flag;
    if (strstr(source, prefix[0]) != NULL){
        flag = 1;
    }else if (strstr(source, prefix[1]) != NULL){
        flag = 2;
    }
    char fileSource[1000], fileDest[1000];
    sprintf(fileSource, "%s%s", dirPath, source);
    sprintf(fileDest, "%s%s", dirPath, dest);
    int result;
    if (flag == 1){
        if (strstr(fileDest, prefix[0]) != NULL)
            wibuLog(fileSource, fileDest, 1);
        else if (strstr(fileSource, prefix[0]) != NULL)
            wibuLog(fileSource, fileDest, 2);
        result = rename(fileSource, fileDest);
    }else if(flag == 2){
        char *strCheck; 
        strCheck = strstr(fileSource, prefix[1]);
        if (strCheck != NULL)
            decode2(strCheck, 0);
        strCheck = strstr(fileDest, prefix[1]);
        if (strCheck != NULL)
            decode2(strCheck, 0);
        printf("RENAME-------\n");
        printf("%s\n", fileSource);
        printf("%s\n", fileDest);
        if ((result = rename(fileSource, fileDest)) == 0){
            char temp[2*v];
            sprintf(temp, "RENAME::%s::%s", fileSource, fileDest);
            hayolongapainLog(temp, INFO);
        }
    }

    if (result == -1)
        return -errno;

    return 0;
}

static int xmp_open(const char *path, struct fuse_file_info *fi){
    char *strCheck;
    if ((strCheck = strstr(path, prefix[0])) != NULL){
        if (strCheck != NULL)
            decode1(strCheck);
    }else if ((strCheck = strstr(path, prefix[1])) != NULL){
        if (strCheck != NULL)
            decode2(strCheck, 0);
    }

    char fpath[1000];
    if (strcmp(path, "/") == 0){
        path = dirPath;
        sprintf(fpath, "%s", path);
    }
    else
        sprintf(fpath, "%s%s", dirPath, path);
    
    int result;
    result = open(fpath, fi->flags);
    if (result == -1)
        return -errno;
    close(result);
    return 0;
}

static int xmp_write(const char *path, const char *buf, size_t size, off_t offset, struct fuse_file_info *fi){
    char *strCheck;
    if ((strCheck = strstr(path, prefix[0])) != NULL){
        if (strCheck != NULL)
            decode1(strCheck);
    }else if ((strCheck = strstr(path, prefix[1])) != NULL){
        if (strCheck != NULL)
            decode2(strCheck, 0);
    }

    char fpath[1000];
    if (strcmp(path, "/") == 0){
        path = dirPath;
        sprintf(fpath, "%s", path);
    }
    else
        sprintf(fpath, "%s%s", dirPath, path);

    int result;
    int fd;
    (void)fi;

    fd = open(fpath, O_WRONLY);
    if (fd == -1)
        return -errno;

    result = pwrite(fd, buf, size, offset);
    if (result == -1)
        result = -errno;

    close(fd);
    return result;
}

static int xmp_fsync(const char *path, int isdatasync, struct fuse_file_info *fi){
    (void)path;
    (void)isdatasync;
    (void)fi;
    return 0;
}

// static int xmp_truncate(const char *path, off_t size){
//     char *strCheck;
//     if ((strCheck = strstr(path, prefix[0])) != NULL){
//         if (strCheck != NULL)
//             decode1(strCheck);
//     }else if ((strCheck = strstr(path, prefix[1])) != NULL){
//         if (strCheck != NULL)
//             decode2(strCheck, 0);
//     }

//     char fpath[1000];
//     if (strcmp(path, "/") == 0){
//         path = dirPath;
//         sprintf(fpath, "%s", path);
//     }
//     else
//         sprintf(fpath, "%s%s", dirPath, path);
//     int result = truncate(fpath, size);
//     if (result == -1)
//         return -errno;

//     return 0;
// }

// static int xmp_access(const char *path, int mask){
//     char *strCheck;
//     if ((strCheck = strstr(path, prefix[0])) != NULL){
//         if (strCheck != NULL)
//             decode1(strCheck);
//     }else if ((strCheck = strstr(path, prefix[1])) != NULL){
//         if (strCheck != NULL)
//             decode2(strCheck, 0);
//     }

//     char fpath[1000];
//     if (strcmp(path, "/") == 0){
//         path = dirPath;
//         sprintf(fpath, "%s", path);
//     }
//     else
//         sprintf(fpath, "%s%s", dirPath, path);

//     int result = access(fpath, mask);
//     if (result == -1)
//         return -errno;

//     return 0;
// }

// static int xmp_readlink(const char *path, char *buf, size_t size){
//     char *strCheck;
//     if ((strCheck = strstr(path, prefix[0])) != NULL){
//         if (strCheck != NULL)
//             decode1(strCheck);
//     }else if ((strCheck = strstr(path, prefix[1])) != NULL){
//         if (strCheck != NULL)
//             decode2(strCheck, 0);
//     }

//     char fpath[1000];
//     if (strcmp(path, "/") == 0){
//         path = dirPath;
//         sprintf(fpath, "%s", path);
//     }
//     else
//         sprintf(fpath, "%s%s", dirPath, path);

//     int result = readlink(fpath, buf, size - 1);
//     if (result == -1)
//         return -errno;

//     buf[result] = '\0';
//     return 0;
// }

// static int xmp_symlink(const char *target, const char *linkpath){
//     int result = symlink(target, linkpath);
//     if (result == -1)
//         return -errno;

//     return 0;
// }

// static int xmp_link(const char *target, const char *linkpath){
//     int result = link(target, linkpath);
//     if (result == -1)
//         return -errno;

//     return 0;
// }

// static int xmp_statfs(const char *path, struct statvfs *stbuf){
//     char *strCheck;
//     if ((strCheck = strstr(path, prefix[0])) != NULL){
//         if (strCheck != NULL)
//             decode1(strCheck);
//     }else if ((strCheck = strstr(path, prefix[1])) != NULL){
//         if (strCheck != NULL)
//             decode2(strCheck, 0);
//     }

//     char fpath[1000];
//     if (strcmp(path, "/") == 0){
//         path = dirPath;
//         sprintf(fpath, "%s", path);
//     }
//     else
//         sprintf(fpath, "%s%s", dirPath, path);

//     int result = statvfs(fpath, stbuf);
//     if (result == -1)
//         return -errno;

//     return 0;
// }

static struct fuse_operations xmp_oper = {
    .getattr = xmp_getattr,
    .readdir = xmp_readdir,
    .read = xmp_read,
    .mkdir = xmp_mkdir,
    .mknod = xmp_mknod,
    .unlink = xmp_unlink,
    .rmdir = xmp_rmdir,
    .rename = xmp_rename,
    .open = xmp_open,
    .write = xmp_write,
    .fsync = xmp_fsync,
    // .truncate = xmp_truncate,
    // .access = xmp_access,
    // .readlink = xmp_readlink,
    // .symlink = xmp_symlink,
    // .link = xmp_link,
    // .statfs = xmp_statfs,
};

int main(int argc, char *argv[]){
    umask(0);
    return fuse_main(argc, argv, &xmp_oper, NULL);
}

